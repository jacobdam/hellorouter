/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TabBarIOS,
  SegmentedControlIOS,
  Button,
  TextInput
} from 'react-native';
import {createMemoryHistory} from 'react-router';

const history = createMemoryHistory('/');

class PageA extends Component {
  render() {
    return <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}><Text>Page A</Text></View>
  }
}

class PageB extends Component {
  render() {
    return <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}><Text>Page B</Text></View>
  }
}

class PageTabB extends Component {
  state = {
    selectedIndex: 0
  }

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <SegmentedControlIOS
          values={['One', 'Two']}
          selectedIndex={this.state.selectedIndex}
          onChange={(event) => {
            this.setState({selectedIndex: event.nativeEvent.selectedSegmentIndex});
          }}>
        </SegmentedControlIOS>
        {this.props.children}
      </View>
    );
  }
}


export default class hellorouter extends Component {
  state = {
    selectedTab: 'tabA'
  };

  // render() {
  //   return (
  //     <TabBarIOS
  //       unselectedTintColor="yellow"
  //       tintColor="white"
  //       barTintColor="darkslateblue"
  //       translucent={true}>
  //
  //       <TabBarIOS.Item
  //         title="AAA"
  //         selected={this.state.selectedTab=='tabA'}
  //         onPress={(ev) => { this.setState({selectedTab: 'tabA'}); }}>
  //
  //         <PageA />
  //       </TabBarIOS.Item>
  //
  //       <TabBarIOS.Item
  //         title="BBB"
  //         selected={this.state.selectedTab=='tabB'}
  //         onPress={(ev) => { this.setState({selectedTab: 'tabB'}); }}>
  //
  //         <PageTabB />
  //       </TabBarIOS.Item>
  //
  //     </TabBarIOS>
  //   );
  // }
  render() {
    return (
      <View style={{flex:1, flexDirection: 'column'}}>
        <NativeAddressBar history={history}/>
        <PageTabB />
      </View>
    )
  }
}

class NativeAddressBar extends Component {
  static propTypes = {
    history: React.PropTypes.object
  }

  state = {
    canGoBack: false,
    canGoForward: false,
    uriText: '/',
    uri: '/',
    isUriEditting: false
  }

  render() {
    return (
      <View style={{flex: 0, flexDirection: 'row', height: 32, marginTop: 15}}>
        <Button title={'<'} disabled={!this.state.canGoBack} onPress={()=>{ this.props.history.goBack(); }}/>
        <Button title={'>'} disabled={!this.state.canGoForward} onPress={()=>{ this.props.history.goForward(); }}/>
        <Button title={'H'} onPress={()=>{this._pushUri('/')}}/>
        <View style={{flex:1, padding: 4}}>
          <TextInput
            style={{flex: 1, borderWidth: 1, borderColor : 'black'}}
            multiline={false}
            autoCapitalize={'none'}
            autoCorrect={false}
            blurOnSubmit={true}
            value={this.state.uriText}
            onChangeText={(uriText) => { this.setState({uriText}) }}
            onSubmitEditing={(e)=>{ this._pushUri(this.state.uriText) }}
            onFocus={()=>{ this.setState({isUriEditting: true})}}
            onEndEditing={()=>{ this.setState({isUriEditting: false, uriText: this.state.uri}) }}
          />
        </View>
        <Button title={'G'} onPress={()=>{ this._pushUri(this.state.uriText) }}/>
      </View>
    )
  }

  _pushUri(uriText) {
    const history = this.props.history;
    history.push(uriText);
  }

  componentDidMount() {
    const history = this.props.history
    this.historyUnlisten = history.listen((location, action) => {
      let state = {
        canGoBack: history.canGo(-1),
        canGoForward: history.canGo(1),
        uri: location.pathname,
      };
      if (!this.state.isUriEditting) {
        state.uriText = state.uri;
      }
      this.setState(state);
    });
  }

  componentWillUnmount() {
    this.historyUnlisten();
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('hellorouter', () => hellorouter);
